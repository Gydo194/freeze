package test;

import com.gydo194.freeze.Fixture.FixtureGroupInterface;
import com.gydo194.freeze.Loader.FixtureGroupLoader;

public class Main {

	public static void main(String[] args) {
		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
	}

}
