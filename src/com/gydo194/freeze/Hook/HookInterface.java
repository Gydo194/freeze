package com.gydo194.freeze.Hook;

import com.gydo194.freeze.Exception.HookException;
import com.gydo194.freeze.Fixture.FixtureGroupInterface;

/**
 * The Hook Interface
 * 
 * <p>
 * Hooks can be executed before or after
 * loading a {@link FixtureGroupInterface}.
 * </p>
 * 
 * <p>
 * This can be useful to initialize an ORM framework
 * prior to loading a {@link FixtureGroupInterface}
 * and shutting it down afterwards.
 * </p>
 * 
 * @author Gydo194
 *
 */
public interface HookInterface {
	
	/**
	 * Get the name of this hook
	 * 
	 * @return the name of this hook
	 */
	String getName();

	/**
	 * Runs the hook
	 */
	void run() throws HookException;
}
