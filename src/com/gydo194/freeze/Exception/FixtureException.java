package com.gydo194.freeze.Exception;

import com.gydo194.freeze.Fixture.FixtureInterface;

/**
 * FixtureException
 * 
 * <p>
 * Thrown by the <code>load/code> function of {@link FixtureInterface}
 * when an unrecoverable error is encountered.
 * </p>
 * 
 * @author Gydo194
 *
 */
public class FixtureException extends Exception {
	private static final long serialVersionUID = -5352255160717589882L;
}
