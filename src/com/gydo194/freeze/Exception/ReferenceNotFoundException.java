package com.gydo194.freeze.Exception;

import com.gydo194.freeze.Reference.ReferenceManager;

/**
 * ReferenceNotFoundException
 * 
 * <p>
 * Thrown by {@link ReferenceManager} when an unknown reference is requested.
 * </p>
 * 
 * @author Gydo194
 *
 */
public final class ReferenceNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1925573042117134848L;

	/**
	 * {@link ReferenceNotFoundException} Constructor
	 * 
	 * @param message The exception message
	 */
	private ReferenceNotFoundException(final String message) {
		super(message);
	}

	/**
	 * Create a {@link ReferenceNotFoundException} with the supplied message
	 * 
	 * @param message The exception message to use
	 */
	public static ReferenceNotFoundException withMessage(final String message) {
		return new ReferenceNotFoundException(message);
	}
}
