package com.gydo194.freeze.Exception;

import com.gydo194.freeze.Hook.HookInterface;

/**
 * HookException
 * 
 * <p>
 * Thrown by the <code>run</code> function of {@link HookInterface}
 * when an unrecoverable error is encountered.
 * </p>
 * 
 * @author gydo194
 *
 */
public class HookException extends Exception {
	private static final long serialVersionUID = 5800743493250384340L;
}
