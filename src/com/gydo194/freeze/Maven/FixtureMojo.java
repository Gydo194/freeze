package com.gydo194.freeze.Maven;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.gydo194.freeze.Fixture.FixtureGroupInterface;
import com.gydo194.freeze.Loader.FixtureGroupLoader;

/**
 * Fixture Mojo
 * 
 * <p>
 * Maven goal to load {@link FixtureGroupInterface} objects by their fully qualified class name
 * </p>
 * 
 * <p>
 * To load a {@link FixtureGroupInterface} using the Maven command line, execute
 * 
 * <br>
 * <code>mvn com.gydo194:freeze:loadFixtureGroup -DfixtureGroupClass=&lt;the fully qualified class name&gt;</code>
 * </p>
 * 
 * <p>
 * <h3>WARNING</h3>
 * This feature is not complete yet, do rely on it.
 * </p>
 *
 * @author gydo194
 *
 */
@Mojo(name = "loadFixtureGroup", defaultPhase = LifecyclePhase.INITIALIZE)
public final class FixtureMojo extends AbstractMojo {

	/**
	 * The current maven project
	 */
	@Parameter(property = "project", readonly = true)
	private MavenProject project;

	/**
	 * The fully qualified class name of the {@link FixtureGroupInterface} to load
	 */
	@Parameter(property = "fixtureGroupClass", required = true)
	private String fixtureGroupClassName;

	private static final Logger logger = LoggerFactory.getLogger(FixtureMojo.class);

	/* https://stackoverflow.com/questions/49737706/access-project-classes-from-a-maven-plugin */
	private ClassLoader getClassLoader() {
		try {
			List<String> classpathElements = project.getCompileClasspathElements();
			classpathElements.add(project.getBuild().getOutputDirectory());
			classpathElements.add(project.getBuild().getTestOutputDirectory());

			URL urls[] = new URL[classpathElements.size()];
			for (int i = 0; i < classpathElements.size(); ++i) {
				urls[i] = new File((String) classpathElements.get(i)).toURI().toURL();
			}

			return new URLClassLoader(urls, this.getClass().getClassLoader());
		} catch (Exception e) {
			logger.error("Failed to get class loader. Loading of project classes is likely to fail.");
			return this.getClass().getClassLoader();
		}
	}

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		logger.info("Freeze: Attempting to load fixture group class {}", fixtureGroupClassName);
		try {
			FixtureGroupInterface fixtureGroup = (FixtureGroupInterface) getClassLoader().loadClass(fixtureGroupClassName).newInstance();
			logger.info("Attempting to load fixture group '{}'", fixtureGroup.getName());
			FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		} catch (InstantiationException e) {
			logger.error("Loading of fixture group '{}' failed: Class instantiation failed", fixtureGroupClassName);
			throw new MojoExecutionException("Class instantiation failed", e);
		} catch (IllegalAccessException e) {
			logger.error("Loading of fixture group '{}' failed", fixtureGroupClassName);
			throw new MojoExecutionException("Illegal Access", e);
		} catch (ClassNotFoundException e) {
			logger.error("Loading of fixture group '{}' failed: Class not found", fixtureGroupClassName);
			throw new MojoExecutionException("Class not found", e);
		}
	}
}