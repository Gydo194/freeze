package com.gydo194.freeze.Hook;

import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.HookException;

/**
 * Service class for executing {@link HookInterface} objects
 * 
 * @author Gydo194
 *
 */
public final class HookRunner {
	private static final Logger logger = LoggerFactory.getLogger(HookRunner.class);

	private static HookRunner instance;
	
	/**
	 * Returns an instance of {@link HookRunner}
	 * 
	 * @return {@link HookRunner}
	 */
	public static HookRunner getInstance() {
		if(null == instance) {
			instance = new HookRunner();
		}
		
		return instance;
	}
	
	private HookRunner() {
		
	}
	
	/**
	 * Execute a singla hook
	 * 
	 * @param hook {@link HookInterface} to be executed
	 */
	private void runHook(final HookInterface hook) {
		try {
			hook.run();
		}
		catch(HookException hookException) {
			logger.error("Failed to run hook '{}':", hook.getName());
			hookException.printStackTrace();
		}
		catch(NullPointerException nullPointerException) {
			logger.error("Failed to run hook: NULL given");
		}
	}
	
	/**
	 * Execute multiple hooks
	 * 
	 * @param hooks a {@link Queue} of {@link HookInterface} objects to be executed
	 */
	public void runHooks(final Queue<HookInterface> hooks) {
		try {
			for(final HookInterface currentHook : hooks) {
				runHook(currentHook);
			}
		}
		catch(NullPointerException nullPointerException) {
		}
	}
}