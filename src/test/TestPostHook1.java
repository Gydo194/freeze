package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.HookException;
import com.gydo194.freeze.Hook.HookInterface;

public final class TestPostHook1 implements HookInterface {

	private static final Logger logger = LoggerFactory.getLogger(TestPostHook1.class);

	@Override
	public String getName() {
		return "Test Post Hook 1";
	}

	@Override
	public void run() throws HookException {
		Test.getInstance().registerCall("TestPostHook1::run");
		logger.info("Test Post Hook 1 run");
	}
}