package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.FixtureException;
import com.gydo194.freeze.Fixture.FixtureInterface;

public class DummyFixture1 implements FixtureInterface {

	private static final Logger logger = LoggerFactory.getLogger(DummyFixture1.class);

	@Override
	public void load() throws FixtureException {
		logger.info("Dummy fixture 1 loaded!");
		Test.getInstance().registerCall("DummyFixture1::load");
		throw new FixtureException();
	}

	@Override
	public String getName() {
		return "Dummy Fixture 1";
	}

}
