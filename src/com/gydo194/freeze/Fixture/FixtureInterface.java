package com.gydo194.freeze.Fixture;

import com.gydo194.freeze.Exception.FixtureException;

/**
 * 
 * The Fixture Interface
 * 
 * <p>
 * Fixtures can be used to load required default data
 * or test data into your local database.
 * </p>
 * 
 * <p>
 * The {@link FixtureInterface} needs to be part
 * of a {@link FixtureGroupInterface} to be loaded.
 * </p>
 * 
 * @author Gydo194
 */
public interface FixtureInterface {

	/**
	 * Returns this fixture's name
	 * 
	 * @return this fixture's name
	 */
	public String getName();

	/**
	 * Loads the fixture
	 * 
	 * @throws FixtureException
	 */
	public void load() throws FixtureException;
}
