package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.FixtureException;
import com.gydo194.freeze.Fixture.FixtureInterface;

public final class DummyFixture2 implements FixtureInterface {

	private static final Logger logger = LoggerFactory.getLogger(DummyFixture2.class);

	@Override
	public String getName() {
		return "Dummy Fixture 2";
	}

	@Override
	public void load() throws FixtureException {
		logger.info("Dummy fixture 2 loaded!");
		Test.getInstance().registerCall("DummyFixture2::load");
	}
}