package test;

import java.util.LinkedList;
import java.util.Queue;

import com.gydo194.freeze.Fixture.FixtureGroupInterface;
import com.gydo194.freeze.Fixture.FixtureInterface;
import com.gydo194.freeze.Hook.HookInterface;

public class DummyFixtureGroupA implements FixtureGroupInterface {

	@Override
	public String getName() {
		return "Dummy Fixture Group A";
	}

	@Override
	public Queue<HookInterface> getPreExecutionHooks() {
		Queue<HookInterface> preHooks = new LinkedList<HookInterface>();

		preHooks.add(new TestPreHook0());
		preHooks.add(new TestPreHook1());

		return preHooks;
	}

	@Override
	public Queue<HookInterface> getPostExecutionHooks() {
		Queue<HookInterface> postHooks = new LinkedList<HookInterface>();

		postHooks.add(new TestPostHook0());
		postHooks.add(new TestPostHook1());

		return postHooks;
	}

	@Override
	public Queue<FixtureInterface> getFixtures() {
		Queue<FixtureInterface> fixtures = new LinkedList<FixtureInterface>();

		fixtures.add(new DummyFixture0());
		fixtures.add(new DummyFixture1());
		fixtures.add(new DummyFixture2());

		return fixtures;
	}

}
