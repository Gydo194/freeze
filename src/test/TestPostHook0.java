package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.HookException;
import com.gydo194.freeze.Hook.HookInterface;

public final class TestPostHook0 implements HookInterface {

	private static final Logger logger = LoggerFactory.getLogger(TestPreHook0.class);

	@Override
	public String getName() {
		return "Test Post Hook 0";
	}

	@Override
	public void run() throws HookException {
		Test.getInstance().registerCall("TestPostHook0::run");
		logger.info("Test Post Hook 0 run");
	}
}