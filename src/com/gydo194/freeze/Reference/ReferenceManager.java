package com.gydo194.freeze.Reference;

import java.util.HashMap;
import java.util.Map;

import com.gydo194.freeze.Exception.ReferenceNotFoundException;

/**
 * ReferenceManager
 * 
 * <p>
 * Manage object references between fixtures
 * </p>
 * 
 * @since 1.7
 * 
 * @author gydo194
 */
public final class ReferenceManager {
	private static ReferenceManager instance;
	
	private Map<String, Object> references;
	
	/**
	 * Returns an instance of {@link ReferenceManager}
	 * 
	 * @return {@link ReferenceManager}
	 */
	public static ReferenceManager getInstance() {
		if(null == instance) {
			instance = new ReferenceManager();
		}
		
		return instance;
	}
	
	private ReferenceManager() {
		references = new HashMap<String, Object>();
	}

	/**
	 * Set a reference to an object
	 * 
	 * @param key The key under which to store this object
	 * @param value The value to store
	 */
	public void setReference(final String key, Object value) {
		references.put(key, value);
	}
	
	/**
	 * Retrieve a referenced object
	 * 
	 * @param key The key referring to the object you want to retrieve
	 * @return The object referred to, or <code>null</code> if no object is found
	 * @throws ReferenceNotFoundException when the requested reference does not exist
	 */
	public Object getReference(final String key) throws ReferenceNotFoundException {
		if(!references.containsKey(key)) {
			throw ReferenceNotFoundException.withMessage(
					String.format("Reference '%s' not found", key)
			);
		}
		return references.get(key);
	}
}
