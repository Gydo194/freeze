package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.HookException;
import com.gydo194.freeze.Hook.HookInterface;

public final class TestPreHook1 implements HookInterface {

	private static final Logger logger = LoggerFactory.getLogger(TestPreHook1.class);

	@Override
	public String getName() {
		return "Test Pre Hook 1";
	}

	@Override
	public void run() throws HookException {
		Test.getInstance().registerCall("TestPreHook1::run");
		logger.info("Test Pre Hook 1 run");
	}
}