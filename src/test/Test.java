package test;

import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.ReferenceNotFoundException;
import com.gydo194.freeze.Fixture.FixtureGroupInterface;
import com.gydo194.freeze.Loader.FixtureGroupLoader;
import com.gydo194.freeze.Reference.ReferenceManager;

public final class Test {
	public static int EXIT_FAILURE = 255;

	private static Test instance;
	private static final Logger logger = LoggerFactory.getLogger(Test.class);
	
	private Stack<String> calls;
	
	public static Test getInstance() {
		if(null == instance) {
			instance = new Test();
		}
		
		return instance;
	}
	
	private Test() {
		calls = new Stack<String>();
	}
	
	public void registerCall(final String functionName) {
		calls.push(functionName);
	}
	
	private void assertCalled(final String functionName) {
		assert calls.contains(functionName) : String.format("Function not called: '%s'", functionName);
	}
	
	/**
	 * Assert function A is called before function B
	 * 
	 * @param functionNameA
	 * @param functionNameB
	 */
	private void assertCalledBefore(final String functionNameA, final String functionNameB) {
		assert calls.indexOf(functionNameA) < calls.indexOf(functionNameB) :
			String.format("Failed asserting that function '%s' is called before '%s'",
					functionNameA,
					functionNameB
			);
	}
	
	private void listCalls() {
		for(String functionName : calls) {
			logger.info("Call: {}", functionName);
		}
	}
	
	private void resetRegisteredCalls() {
		calls.clear();
	}
	
	public void testFixtureLoading() {
		logger.info("Testing loading of fixtures...");

		resetRegisteredCalls();

		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		
		assertCalled("DummyFixture0::load");
		assertCalled("DummyFixture1::load");
		assertCalled("DummyFixture2::load");

		logger.info("Success!");
	}
	
	public void testFixtureGroupLoadingFromString() {
		logger.info("Testing loading of fixture group from class name string...");

		resetRegisteredCalls();

		FixtureGroupLoader.getInstance().loadFixtureGroupFromString("test.DummyFixtureGroupA");
		
		assertCalled("DummyFixture0::load");
		assertCalled("DummyFixture1::load");
		assertCalled("DummyFixture2::load");

		logger.info("Success!");
	}
	
	public void testFixtureLoadingOrder() {
		logger.info("Testing loading of fixtures in correct order...");

		resetRegisteredCalls();

		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		
		listCalls();
		
		assertCalledBefore("DummyFixture0::load", "DummyFixture1::load");
		assertCalledBefore("DummyFixture1::load", "DummyFixture2::load");

		logger.info("Success!");
	}
	
	public void testReferenceRegistrationSuccessful() {
		logger.info("Testing registration and retrieval of references...");

		resetRegisteredCalls();

		String key = "Test Key";
		String value = "Test value";

		ReferenceManager.getInstance().setReference(key, value);
		try {
			assert value.equals(ReferenceManager.getInstance().getReference(key));
		} catch (ReferenceNotFoundException e) {
			logger.error("testReferenceRegistrationSuccessful: ReferenceManager unexpectedly threw ReferenceNotFoundException");
			e.printStackTrace();
			assert false : "ReferenceManager unexpectedly threw ReferenceNotFoundException";
		}

		logger.info("Success!");
	}
	
	public void testReferenceRegistrationFailure() {
		logger.info("Testing failing reference retrieval...");

		resetRegisteredCalls();

		String key = "Test Key";
		String value = "Test value";

		ReferenceManager.getInstance().setReference(key, value);
		try {
			ReferenceManager.getInstance().getReference("nonexistent key");
		} catch(ReferenceNotFoundException exception) {
			logger.info("Success!");
			return;
		}
		
		assert false : "ReferenceManager::getReference did not throw ReferenceNotFoundException";
	}
	
	public void testPreExececutionHooks() {
		logger.info("Testing pre-execution hooks...");
		
		resetRegisteredCalls();
		
		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		
		assertCalled("TestPreHook0::run");
		assertCalled("TestPreHook1::run");
		
		logger.info("Success!");
	}
	
	public void testPostExececutionHooks() {
		logger.info("Testing post-execution hooks...");
		
		resetRegisteredCalls();
		
		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		
		assertCalled("TestPostHook0::run");
		assertCalled("TestPostHook1::run");
		
		logger.info("Success!");
	}
	
	public void testPreExececutionHookExecutionOrder() {
		logger.info("Testing pre-execution hook execution order...");
		
		resetRegisteredCalls();
		
		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		
		assertCalledBefore("TestPreHook0::run", "TestPreHook1::run");

		logger.info("Success!");
	}
	
	public void testPostExececutionHookExecutionOrder() {
		logger.info("Testing post-execution hook execution order...");
		
		resetRegisteredCalls();
		
		FixtureGroupInterface fixtureGroup = new DummyFixtureGroupA();
		FixtureGroupLoader.getInstance().loadFixtureGroup(fixtureGroup);
		
		assertCalledBefore("TestPostHook0::run", "TestPostHook1::run");

		logger.info("Success!");
	}
	
	public boolean runTests() {
		try {
			Test.getInstance().testFixtureLoading();
			Test.getInstance().testFixtureGroupLoadingFromString();
			Test.getInstance().testFixtureLoadingOrder();
			Test.getInstance().testReferenceRegistrationSuccessful();
			Test.getInstance().testReferenceRegistrationFailure();
			Test.getInstance().testPreExececutionHooks();
			Test.getInstance().testPostExececutionHooks();
			Test.getInstance().testPreExececutionHookExecutionOrder();
			Test.getInstance().testPostExececutionHookExecutionOrder();
		} catch(Throwable throwable) { //Throwable also catches assertions whereas Exception does not
			return false;
		}

		return true;
	}
	
	public static void main(String[] args) {
		if(Test.getInstance().runTests()) {
			logger.info("All tests passed!");
			return;
		} else {
			logger.error("Tests have failed!");
			System.exit(Test.EXIT_FAILURE);
		}
	}
}
