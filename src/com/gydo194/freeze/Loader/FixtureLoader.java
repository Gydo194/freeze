package com.gydo194.freeze.Loader;

import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.FixtureException;
import com.gydo194.freeze.Fixture.FixtureInterface;

/**
 * Service class to load {@link FixtureInterface} objects
 * 
 * @author Gydo194
 *
 */
public class FixtureLoader {
	private static final Logger logger = LoggerFactory.getLogger(FixtureLoader.class);

	private static FixtureLoader instance;
	
	/**
	 * Returns a {@link FixtureLoader} instance
	 * 
	 * @return {@link FixtureLoader}
	 */
	public static FixtureLoader getInstance() {
		if(null == instance) {
			instance = new FixtureLoader();
		}
		
		return instance;
	}
	
	private FixtureLoader() {
		
	}
	
	/**
	 * Loads a {@link FixtureInterface}
	 * 
	 * Shadows any thrown FixtureException
	 * 
	 * @param fixture The {@link FixtureInterface} to load
	 */
	private void loadFixture(FixtureInterface fixture) {
		try {
			fixture.load();
		}
		catch(FixtureException | NullPointerException e) {
			String fixtureName = null == fixture ? "Null" : fixture.getName();
			logger.error("Failed to load fixture '{}'!", fixtureName);
		}
	}

	/**
	 * Loads a {@link Queue} of {@link FixtureInterface} objects
	 * 
	 * @param fixtureGroup {@link Queue} of {@link FixtureInterface} objects
	 */
	public void loadFixtures(Queue<FixtureInterface> fixtures) {
		for(FixtureInterface fixture : fixtures) {
			loadFixture(fixture);
		}
	}
}
