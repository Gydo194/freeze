package test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Exception.FixtureException;
import com.gydo194.freeze.Fixture.FixtureInterface;

public class DummyFixture0 implements FixtureInterface {

	private static final Logger logger = LoggerFactory.getLogger(DummyFixture0.class);

	@Override
	public void load() throws FixtureException {
		logger.info("Dummy fixture 0 loaded!");
		Test.getInstance().registerCall("DummyFixture0::load");
	}

	@Override
	public String getName() {
		return "Dummy Fixture 0";
	}

}
