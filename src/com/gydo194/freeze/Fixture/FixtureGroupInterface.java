package com.gydo194.freeze.Fixture;

import java.util.Queue;

import com.gydo194.freeze.Hook.HookInterface;

/**
 * 
 * The Fixture Group Interface
 * 
 * <p>
 * This interface resembles a fixture group,
 * containing both fixtures and two sets of hooks.
 * One set of hooks will be executed before the fixtures
 * in this group are loaded, and one will be executed afterwards.
 * </p>
 * 
 * <p>
 * Both fixtures and hooks are stored in queues,
 * which preserve insertion order.
 * 
 * This means that fixtures and hooks will be executed
 * in the order as they are inserted into the queue.
 * </p>
 * 
 * @author Gydo194
 */
public interface FixtureGroupInterface {

	/**
	 * Returns the name of the fixture group
	 * 
	 * @return {@link String}
	 */
	String getName();
	
	/**
	 * Returns a list of the fixtures in this group
	 * 
	 * @return a {@link java.util.Queue} of {@link FixtureInterface} objects belonging to this fixture group
	 */
	Queue<FixtureInterface> getFixtures();
	
	/**
	 * Returns a set of pre-execution hooks to be
	 * executed before loading this fixture group.
	 * 
	 * @return A {@link java.util.Queue} of {@link com.gydo194.freeze.Hook.HookInterface}
	 */
	Queue<HookInterface> getPreExecutionHooks();

	/**
	 * Returns a set of post-execution hooks to be
	 * executed after loading this fixture group.
	 * 
	 * @return A {@link java.util.Queue} of {@link com.gydo194.freeze.Hook.HookInterface}
	 */
	Queue<HookInterface> getPostExecutionHooks();
}
