package com.gydo194.freeze.Loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Fixture.FixtureGroupInterface;
import com.gydo194.freeze.Hook.HookRunner;

/**
 * Service class for loading {@link FixtureGroupInterface} objects
 * 
 * @author Gydo194
 *
 */
public class FixtureGroupLoader {
	private static final Logger logger = LoggerFactory.getLogger(FixtureGroupLoader.class);

	private static FixtureGroupLoader instance;
	
	/**
	 * Returns a {@link FixtureGroupLoader} instance
	 * 
	 * @return {@link FixtureGroupLoader}
	 */
	public static FixtureGroupLoader getInstance() {
		if(null == instance) {
			instance = new FixtureGroupLoader();
		}
		
		return instance;
	}
	
	private FixtureGroupLoader() {
		
	}
	
	/**
	 * Loads a {@link FixtureGroupInterface}
	 * 
	 * @param fixtureGroup The {@link FixtureGroupInterface} to load
	 */
	public void loadFixtureGroup(final FixtureGroupInterface fixtureGroup) {
		logger.info("Running pre-execution hooks...");
		HookRunner.getInstance().runHooks(fixtureGroup.getPreExecutionHooks());

		logger.info("Running fixtures...");
		FixtureLoader.getInstance().loadFixtures(fixtureGroup.getFixtures());

		logger.info("Running post-execution hooks...");
		HookRunner.getInstance().runHooks(fixtureGroup.getPostExecutionHooks());
	}
	
	/**
	 * Load a {@link FixtureGroupInterface} by its fully qualified class name.
	 * 
	 * Shadows thrown exceptions and writes to the error log instead.
	 * 
	 * @param fixtureGroupClassName {@link String} The fully qualified class name of the {@link FixtureGroupInterface} to load
	 */
	public void loadFixtureGroupFromString(final String fixtureGroupClassName) {
		try {
			Class<?> fixtureGroupClass = Class.forName(fixtureGroupClassName);
			FixtureGroupInterface fixtureGroup = (FixtureGroupInterface) fixtureGroupClass.newInstance();

			logger.info("Loading fixture group '{}'...", fixtureGroup.getName());
			loadFixtureGroup(fixtureGroup);
			logger.info("Done loading fixture group '{}'!", fixtureGroup.getName());
		}
		catch(ClassNotFoundException exception) {
			logger.error("Failed to load Fixture Group '{}': Class not found", fixtureGroupClassName);
		}
		catch(InstantiationException | IllegalAccessException exception) {
			logger.error("Failed to load Fixture Group '{}': Failed to instantiate fixture class", fixtureGroupClassName);
			exception.printStackTrace();
		}
		catch(NullPointerException nullPointerException) {
			logger.error("Failed to load Fixture Group '{}': Null Pointer Exception", fixtureGroupClassName);
			nullPointerException.printStackTrace();
		}
	}
}
