package com.gydo194.freeze;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gydo194.freeze.Loader.FixtureGroupLoader;

/**
 * Freeze main class.
 * 
 * <p>
 * Invoke the main function of this class
 * with fixture group class names as arguments to load them.
 * </p>
 * 
 * @author Gydo194
 *
 */
public class Freeze {

	private static final Logger logger = LoggerFactory.getLogger(Freeze.class);

	public static void main(String[] args) {
		for(String fixtureGroupClassName : args) {
			logger.info("Attempting to load fixture group '{}'.", fixtureGroupClassName);
			FixtureGroupLoader.getInstance().loadFixtureGroupFromString(fixtureGroupClassName);
		}
	}
}
